package pl.sda.git.impl;

import pl.sda.git.Action;

import java.util.List;
import java.util.stream.Collectors;


//wyswietl wszystkie elementy, ktore maja w sobie slowo kot
public class Michal2 implements Action {
    @Override
    public String doIt(List<String> strings) {
        return strings.stream()
                .filter(x->x.contains("kot"))
                .collect(Collectors.toList()).toString();
    }
}
