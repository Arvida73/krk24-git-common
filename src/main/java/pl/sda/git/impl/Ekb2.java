package pl.sda.git.impl;

import pl.sda.git.Action;

import java.util.List;

public class Ekb2 implements Action {
    @Override
    public String doIt(List<String> strings) {
        int numberOfWordsWithA=0;
        for(String line : strings){
            if(line.contains("a")) numberOfWordsWithA++ ;
        }
        return String.valueOf(numberOfWordsWithA);
    }
}
