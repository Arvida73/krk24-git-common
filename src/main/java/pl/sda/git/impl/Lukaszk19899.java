package pl.sda.git.impl;

import pl.sda.git.Action;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Lukaszk19899 implements Action {
    @Override
    public String doIt(List<String> strings) {
        return strings.stream().sorted(Comparator.comparing(x->x)).collect(Collectors.toList()).toString();
    }
}
