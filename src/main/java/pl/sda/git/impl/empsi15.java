package pl.sda.git.impl;

import org.w3c.dom.ls.LSOutput;
import pl.sda.git.Action;

import java.util.List;

public class empsi15 implements Action {

    @Override
    public String doIt(List<String> strings) {
        Long count=strings.stream()
                .filter(x->x.contains("a"))
                .count();
    return count.toString();
    }

}
