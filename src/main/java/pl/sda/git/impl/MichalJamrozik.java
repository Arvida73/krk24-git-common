package pl.sda.git.impl;

import pl.sda.git.Action;

import java.util.List;
import java.util.stream.Collectors;

public class MichalJamrozik implements Action {

    //dodaj do wszystkich argumentow slowo "java"

    @Override
    public String doIt(List<String> strings) {

        return strings.stream()
               .map(x->x+"JAVA")
                .collect(Collectors.toList()).toString();
    }
}
