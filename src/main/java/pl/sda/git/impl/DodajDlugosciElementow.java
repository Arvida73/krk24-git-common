package pl.sda.git.impl;

        import pl.sda.git.Action;

        import java.util.List;


public class DodajDlugosciElementow implements Action {
    @Override
    public String doIt(List<String> strings) {
        return String.valueOf(strings.stream()
                .map(x->x.length())
                .reduce(Integer::sum));
    }
}
