package pl.sda.git.impl;

import pl.sda.git.Action;

import java.util.List;

public class HelloWorld implements Action {

    @Override
    public String doIt(List<String> strings) {
        System.out.println("Hello World");
        return null;
    }
}
