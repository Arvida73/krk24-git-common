package pl.sda.git;

import pl.sda.git.impl.*;
import pl.sda.git.impl.*;
import pl.sda.git.impl.AleksanderDotCom;
import pl.sda.git.impl.ConcatAction;
import pl.sda.git.impl.Ekb;
import pl.sda.git.impl.PatrykImpl;
import pl.sda.git.impl.MichalJamrozik;
import pl.sda.git.impl.Lukaszk19899;
import pl.sda.git.impl.SortAlphaAction;
import pl.sda.git.impl.tpietruszka10;

import java.util.*;

public class Main {


    private static final Map<String, Action> possibleActions = new HashMap<>();


    static {
        possibleActions.put("concat", new ConcatAction());
        possibleActions.put("jp-sort-alpha", new SortAlphaAction());
        possibleActions.put("mj", new MichalJamrozik());
        possibleActions.put("MP",new empsi15());
        possibleActions.put("tpietruszka", new tpietruszka10());
        possibleActions.put("pmarcisz", new PatrykImpl());
        possibleActions.put("EKB",new Ekb());
        possibleActions.put("alpha-sort",new Coresky());

        possibleActions.put("as",new AleksanderDotCom());
        possibleActions.put("damkop", new damkop());
        possibleActions.put("lkozlowski", new Lukaszk19899());
        possibleActions.put("EKB2", new Ekb2());
        possibleActions.put("DodajDlugosciElementow", new DodajDlugosciElementow());
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        System.out.println("Possible actions: ");
        possibleActions.entrySet().stream()
                .map(Map.Entry::getKey)
                .map(name -> "\t" + name)
                .forEach(System.out::println);

        System.out.println("Which action do you want to perform?");
        final String actionName = scanner.nextLine();

        System.out.println("What are the arguments?");
        final String argumentLine = scanner.nextLine();
        final List<String> arguments = Arrays.asList(argumentLine.split(" "));

        final Action action = possibleActions.get(actionName);
        final String result = action.doIt(arguments);
        System.out.println(result);



    }
}
