package pl.sda.git;

import java.util.List;

public interface Action  {
    public String doIt(List<String> strings);

}
